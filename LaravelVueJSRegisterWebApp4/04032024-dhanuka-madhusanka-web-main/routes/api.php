<?php

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/

Route::get('/scheduler', function() {
    Artisan::call('task:delete_archived');
});

Route::prefix('auth')->group(function () {
    Route::post('register', 'AuthController@register');
    Route::post('login', 'AuthController@login');
    Route::get('refresh', 'AuthController@refresh');

    Route::group(['middleware' => 'auth:api'], function () {
        Route::post('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user');
    });
});

Route::group(['middleware' => 'auth:api'], function () {
    Route::apiResource('users', 'UserController');

    Route::get('tasks/{page}/{limit}', 'TaskController@index');
    Route::post('tasks', 'TaskController@store');
    Route::get('tasks/{id}', 'TaskController@show');
    Route::put('tasks/{id}', 'TaskController@update');
    Route::delete('tasks/{id}', 'TaskController@destroy');
    Route::patch('tasks/{id}', 'TaskController@setToComplete');
    Route::post('tasks/upload/{id}', 'TaskController@upload');
    Route::post('tasks/add-tags/{id}', 'TaskController@addMultipleTags');
    Route::patch('tasks/archive/{id}', 'TaskController@setToArchive');

    Route::get('tags', 'TagController@index');
});

//filter and pagination
Route::get('/user/pagnination', 'UserController@pagnination');