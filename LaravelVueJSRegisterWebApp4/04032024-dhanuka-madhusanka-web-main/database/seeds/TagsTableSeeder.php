<?php

use App\Tag;
use Illuminate\Database\Seeder;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Tag::create([
            'tag_name' => 'Test 1'
        ]);

        Tag::create([
            'tag_name' => 'Test 2'
        ]);

        Tag::create([
            'tag_name' => 'Test 3'
        ]);
    }
}
