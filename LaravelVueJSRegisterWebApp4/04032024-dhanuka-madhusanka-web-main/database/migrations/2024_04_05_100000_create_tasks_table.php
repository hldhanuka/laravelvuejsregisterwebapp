<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->bigIncrements('id', true);

            $table->string('title', 150)
                ->comment('Task Title')
                ->index();
            $table->string('description', 500)
                ->comment('Task Description');

            $table->date('due_date')
                ->nullable()
                ->comment('Task Due Date')
                ->index();

            $table->tinyInteger('status')
                ->comment('1 - To-Do, 2 - Completed')
                ->index();
            $table->tinyInteger('priority')
                ->comment('1 - Urgent, 2 - High, 3 - Normal, 4 - Low, 5 - None')
                ->index();

            $table->bigInteger('order')
                ->comment('Task Order Base on User');

            $table->dateTime('archived_at')
                ->nullable()
                ->comment('Archived Date')
                ->index();
            $table->dateTime('completed_at')
                ->nullable()
                ->comment('Completed Date')
                ->index();

            $table->unsignedBigInteger('user_id')
                ->comment('User from users table')
                ->index();

            $table->timestamps();
            
            $table->foreign('user_id')->references('id')->on('users')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}