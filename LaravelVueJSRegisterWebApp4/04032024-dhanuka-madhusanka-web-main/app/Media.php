<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    /**
     * The attribute for the table name.
     *
     * @var string
     */
    protected $table = 'medias';

    /**
     * Set File Name Attribute without file path
     * 
     * @param string $value
     * 
     * @return void
     */
    public function setFileNameAttribute($value)
    {
        $this->attributes['file_name'] = explode("/", $value)[1];
    }

    /**
     * Get File Name Attribute with file path
     * 
     * @param string $value
     * 
     * @return string
     */
    public function getFileNameAttribute($value)
    {
        return self::FILE_SAVE_PATH . '/' . $value;;
    }

    // file types
    public const FILE_TYPE_IMAGES = 1;
    public const FILE_TYPE_VIDEOS = 2;
    public const FILE_TYPE_DOCUMENTS = 2;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'file_name',
        'type',
        'task_id',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'date:Y-m-d H:i:s',
        'updated_at' => 'date:Y-m-d H:i:s',
    ];

    /**
     * Belongs Media to Task
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function task()
    {
        return $this->belongsTo(Task::class, 'task_id');
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////// Helping Methods And Variables
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // media save path name
    public const FILE_SAVE_PATH = "media";

    // task status types text
    public const FILE_TYPE_TXT = [
        self::FILE_TYPE_IMAGES => "Image",
        self::FILE_TYPE_VIDEOS => "Video",
        self::FILE_TYPE_DOCUMENTS => "Document"
    ];

    // allowed image extension
    public const ALLOWED_IMAGE_EXTENSIONS = ['jpg', 'png', 'svg'];

    // allowed video extension
    public const ALLOWED_VIDEO_EXTENSIONS = ['mp4'];

    // allowed document extension
    public const ALLOWED_DOCUMENT_EXTENSIONS = ['csv', 'txt', 'doc', 'docx'];

    /**
     * Getting the all meme types to one array
     * 
     * @return array
     */
    public static function getStringToValidateMemesFromArrayKey() : array
    {
        return array_merge(
            self::ALLOWED_IMAGE_EXTENSIONS, 
            self::ALLOWED_VIDEO_EXTENSIONS, 
            self::ALLOWED_DOCUMENT_EXTENSIONS
        );
    }

    /**
     * Getting File Upload Path
     * 
     * @param string $fileName
     * 
     * @return string
     */
    public static function getMediaFilePath($fileName) : string
    {
        return storage_path('app/' . $fileName);
    }
}
