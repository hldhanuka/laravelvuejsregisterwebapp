<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;

class AuthController extends Controller
{
    /**
     * User Login
     * 
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     * 
     * @OA\Post(
     *     path="/api/auth/login",
     *     summary="Login a user",
     *     tags={"Auth APIs"},
     *     @OA\Parameter(
     *          name="Content-Type",
     *          in="header",
     *          required=true,
     *          example="application/json",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Parameter(
     *         name="email",
     *         in="query",
     *         description="User's email",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="password",
     *         in="query",
     *         description="User's password",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *          response="200", 
     *          description="User Login successfully",
     *          content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="status",
     *                         type="string",
     *                         description="success"
     *                     ),
     *                     example={
     *                         "status": "success"
     *                     }
     *                 )
     *             )
     *         }
     *     ),
     *     @OA\Response(
     *          response="422", 
     *          description="User Login Validation errors",
     *          content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="status",
     *                         type="string",
     *                         description="error"
     *                     ),
     *                     @OA\Property(
     *                         property="error",
     *                         type="string",
     *                         description="Login Validation Error"
     *                     ),
     *                     @OA\Property(
     *                         property="errors",
     *                         type="array",
     *                         description="Validation Errors",
     *                         @OA\Items(
     *                             type="array",
     *                             description="Validation Key Params",
     *                             @OA\Items(
     *                                 type="string",
     *                                 description="Validation Error Message"
     *                             )
     *                         )
     *                     ),
     *                     example={
     *                          "status": "error",
     *                          "error": "Login Validation Error",
     *                          "errors": {
     *                              "email": {
     *                                  "The email field is required."
     *                              },
     *                              "password": {
     *                                  "The password field is required."
     *                              }
     *                          }
     *                     }
     *                 )
     *             )
     *         }
     *     )
     * )
     * 
     */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255',
            'password' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'error' => 'Login Validation Error',
                'errors' => $validator->errors()
            ], 422);
        }

        try {
            $resDetails = $request->only('email', 'password');

            $user = User::where('email', $resDetails['email'])->first();

            if (!$user) {
                return response()->json(['error' => 'Email does not exist'], 404);
            }

            if (!$token = $this->guard()->attempt($resDetails)) {
                return response()->json(['error' => 'Invalid Credentials'], 422);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'Could Not Create Token'], 500);
        }

        return response()->json(['status' => 'success'], 200)->header('Authorization', $token);
    }

    /**
     * User Logout
     *
     * @return \Illuminate\Http\JsonResponse
     * 
     * @OA\Post(
     *     path="/api/auth/logout",
     *     summary="Logout the user",
     *     tags={"Auth APIs"},
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *          name="Content-Type",
     *          in="header",
     *          required=true,
     *          example="application/json",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *          response="204", 
     *          description="User Logout successfully",
     *          content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     example={
     *                     }
     *                 )
     *             )
     *         }
     *     ),
     *     @OA\Response(
     *          response="401", 
     *          description="Try to logout for the users already logged out",
     *          content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="error",
     *                         type="string",
     *                         description="Unauthorized"
     *                     ),
     *                     example={
     *                          "error": "Unauthorized"
     *                     }
     *                 )
     *             )
     *         }
     *     )
     * )
     * 
     */
    public function logout()
    {
        $this->guard()->logout();

        return response()->json([
            'status' => 'success',
            'msg' => 'Logged out Successfully.'
        ], 204);
    }

    /**
     * User Register
     * 
     * @param Request $request
     * 
     * @return \Illuminate\Http\JsonResponse
     * 
     * @OA\Post(
     *     path="/api/auth/register",
     *     summary="Register a new user",
     *     tags={"Auth APIs"},
     *     @OA\Parameter(
     *          name="Content-Type",
     *          in="header",
     *          required=true,
     *          example="application/json",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Parameter(
     *         name="name",
     *         in="query",
     *         description="User's name",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="email",
     *         in="query",
     *         description="User's email",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="password",
     *         in="query",
     *         description="User's password",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *          response="200", 
     *          description="User registered successfully",
     *          content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="status",
     *                         type="string",
     *                         description="success"
     *                     ),
     *                     example={
     *                         "status": "success"
     *                     }
     *                 )
     *             )
     *         }
     *     ),
     *     @OA\Response(
     *          response="422", 
     *          description="Registration Validation errors",
     *          content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="status",
     *                         type="string",
     *                         description="error"
     *                     ),
     *                     @OA\Property(
     *                         property="error",
     *                         type="string",
     *                         description="Registration Validation Error"
     *                     ),
     *                     @OA\Property(
     *                         property="errors",
     *                         type="array",
     *                         description="Validation Errors",
     *                         @OA\Items(
     *                             type="array",
     *                             description="Validation Key Params",
     *                             @OA\Items(
     *                                 type="string",
     *                                 description="Validation Error Message"
     *                             )
     *                         )
     *                     ),
     *                     example={
     *                          "status": "error",
     *                          "error": "Registration Validation Error",
     *                          "errors": {
     *                              "name": {
     *                                  "The name field is required."
     *                              },
     *                              "email": {
     *                                  "The email field is required."
     *                              },
     *                              "password": {
     *                                  "The password field is required."
     *                              }
     *                          }
     *                     }
     *                 )
     *             )
     *         }
     *     )
     * )
     * 
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'error' => 'Registration Validation Error',
                'errors' => $validator->errors()
            ], 422);
        }

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'ip_address' => $_SERVER['HTTP_CLIENT_IP'] ?? $_SERVER['HTTP_X_FORWARDED_FOR'] ?? $_SERVER['REMOTE_ADDR'] ?? '0.0.0.0'
        ]);

        // return response()->json(['status' => 'success'], 200);

        //$token = JWTAuth::fromUser($user);
        //return response()->json(compact('user','token'),201);

        $token = JWTAuth::fromUser($user);

        return response()->json(['status' => 'success'], 200)->header('Authorization', $token);
    }

    /**
     * Get User
     * 
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function user(Request $request)
    {
        $user = User::find(Auth::user()->id);

        return response()->json([
            'status' => 'success',
            'data' => $user
        ]);
    }

    /**
     * Get User Refresh
     *
     * @return \Illuminate\Http\JsonResponse
     * 
     * @OA\Get(
     *     path="/api/auth/refresh",
     *     summary="Refresh the User Token",
     *     security={{"bearerAuth":{}}},
     *     tags={"Auth APIs"},
     *     @OA\Parameter(
     *          name="Content-Type",
     *          in="header",
     *          required=true,
     *          example="application/json",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *          response="200", 
     *          description="Refresh the token successfully",
     *          content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="status",
     *                         type="string",
     *                         description="success"
     *                     ),
     *                     example={
     *                         "status": "success"
     *                     }
     *                 )
     *             )
     *         }
     *     ),
     *     @OA\Response(
     *          response="401", 
     *          description="Try to get the token refreshed for the users already logged out",
     *          content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="error",
     *                         type="string",
     *                         description="Unauthorized"
     *                     ),
     *                     example={
     *                          "error": "Unauthorized"
     *                     }
     *                 )
     *             )
     *         }
     *     )
     * )
     * 
     */
    public function refresh()
    {
        // get the current logged user
        $currentUser = Auth::user();

        if($currentUser == null){
            return response()->json([
                    'error' => 'Unauthorized Request'
                ], 
                401
            );
        }

        if ($token = $this->guard()->refresh()) {
            return response()
                ->json(['status' => 'successs'], 200)
                ->header('Authorization', $token);
        }

        return response()->json(['error' => 'Refresh Token Error'], 401);
    }

    /**
     * Get User Guard
     *
     * @return \Illuminate\Contracts\Auth\Guard|\Illuminate\Contracts\Auth\StatefulGuard
     */
    private function guard()
    {
        return Auth::guard();
    }

    /**
     * Get User Authenticated
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAuthenticatedUser()
    {
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }
        } catch (TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());
        }

        return response()->json(compact('user'));
    }
}
