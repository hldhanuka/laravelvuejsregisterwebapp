<?php

namespace App\Console\Commands;

use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class DeleteArchivedTask extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'task:delete_archived';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Archived Tasks delete weekly';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // initial data
        $logKey = "DELETE_ARCHIVED_TASK";
        $bulkLimit = 100;
        $startDate = date('Y-m-d 00:00:00', strtotime('-7 days'));
        $currentCount = 0;

        Log::info("$logKey|ok|start_the_proccess|$startDate");

        try {
            // read last 4 months data count
            $totallCount = DB::table('tasks')
                ->select('*')
                ->where('tasks.archived_at', '<', $startDate)
                ->count();

            Log::info("$logKey|ok|total_count|error - " . $totallCount);

            if($totallCount) {
                do {
                    // delete bulk details
                    DB::table('tasks')
                        ->where('tasks.archived_at', '<', $startDate)
                        ->orderby('tasks.archived_at', 'ASC')
                        ->take($bulkLimit)
                        ->delete();

                    $currentCount = $currentCount + $bulkLimit;

                    Log::info("$logKey|ok|current_loop_details|current_count - $currentCount");
                } while ($currentCount <= $totallCount);

                Log::info("$logKey|ok|current_loop_details|final_count - $currentCount");
            } else {
                Log::info("$logKey|ok|nothing_to_delete");
            }
        } catch (Exception $ex) {
            Log::info("$logKey|not_ok|try_catch|error -" . $ex->getMessage());
        }

        Log::info("$logKey|ok|end_the_proccess");
    }
}