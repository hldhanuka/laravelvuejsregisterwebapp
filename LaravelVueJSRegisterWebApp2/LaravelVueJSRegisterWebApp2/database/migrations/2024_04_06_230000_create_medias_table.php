<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMediasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medias', function (Blueprint $table) {
            $table->bigIncrements('id', true);

            $table->string('file_name', 50)
                ->comment('Media File Name');

            $table->tinyInteger('type')
                ->comment('1 - images, 2 - videos, 3 - documents')
                ->index();

            $table->unsignedBigInteger('task_id')
                ->comment('Task from tasks table')
                ->index();

            $table->timestamps();
            
            $table->foreign('task_id')->references('id')->on('tasks')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medias');
    }
}