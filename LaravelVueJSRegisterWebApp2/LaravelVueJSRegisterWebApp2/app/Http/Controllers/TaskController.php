<?php

namespace App\Http\Controllers;

use App\Task;
use App\Media;
use Illuminate\Http\Request;
use App\Helpers\CommonHelper;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     * 
     * @param Request $request
     * @param int $page
     * @param int $limit
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $page, $limit)
    {
        // get Request Data
        $data = $request->all();

        if ($limit <= 0) {
            return response()->json([
                'status' => false,
                'error' => 'Invalid Limit'
            ], 422);
        }

        // get the current logged user
        $currentUser = Auth::user();

        // get user id
        $userId = $currentUser->getAttributes()['id'];

        // get count
        $tasksCount = Task::selectRaw('COUNT(id) AS count')
            ->where('user_id', "=", $userId);

        $allTasks = Task::select('*')->with('medias')->where('user_id', "=", $userId);

        // Check the Search Tags
        if (@$data['search']) {
            // get count
            $tasksCount = $tasksCount
                ->where('priority', '=', $data['search'])
                ->orWhere('status', '=', $data['search'])
                ->orWhere('title', '=', $data['search']);

            $allTasks = $allTasks
                ->where('priority', '=', $data['search'])
                ->orWhere('status', '=', $data['search'])
                ->orWhere('title', '=', $data['search']);
        }

        if (@$data['date_col_name'] && @$data['date_start'] && @$data['date_end']) {
            switch ($data['date_col_name']) {
                case 'due_date':
                    $tasksCount = $tasksCount->whereBetween('due_date', [$data['date_start'], $data['date_end']]);

                    $allTasks = $allTasks
                        ->whereBetween('due_date', [$data['date_start'], $data['date_end']]);
                    break;
                case 'created_at':
                    $tasksCount = $tasksCount->whereBetween('created_at', [$data['date_start'], $data['date_end']]);

                    $allTasks = $allTasks
                        ->whereBetween('created_at', [$data['date_start'], $data['date_end']]);
                    break;
                case 'achieved_at':
                    $tasksCount = $tasksCount->whereBetween('achieved_at', [$data['date_start'], $data['date_end']]);

                    $allTasks = $allTasks
                        ->whereBetween('achieved_at', [$data['date_start'], $data['date_end']]);
                    break;
                case 'completed_at':
                    $tasksCount = $tasksCount->whereBetween('completed_at', [$data['date_start'], $data['date_end']]);

                    $allTasks = $allTasks
                        ->whereBetween('completed_at', [$data['date_start'], $data['date_end']]);
                    break;
                default:
                    break;
            }
        }

        if (@$data['sort_col_name'] && ((@$data['sort_by'] == 'desc') || (@$data['sort_by'] == 'asc'))) {
            switch ($data['sort_col_name']) {
                case 'created_at':
                    $allTasks = $allTasks->orderBy('created_at', $data['sort_by']);
                    break;
                case 'priority':
                    $allTasks = $allTasks->orderBy('priority', $data['sort_by']);
                    break;
                case 'due_date':
                    $allTasks = $allTasks->orderBy('due_date', $data['sort_by']);
                    break;
                case 'status':
                    $allTasks = $allTasks->orderBy('status', $data['sort_by']);
                    break;
                case 'description':
                    $allTasks = $allTasks->orderBy('description', $data['sort_by']);
                    break;
                case 'title':
                    $allTasks = $allTasks->orderBy('title', $data['sort_by']);
                    break;
                default:
                    break;
            }
        }

        $tasksCount = $tasksCount->get()->toArray();
        $tasksCount = $tasksCount[0]['count'];

        // skip calculation
        $skip = ((int)$page - 1) * (int)$limit;

        // get the list
        $tasks = $allTasks
            ->skip($skip)
            ->take($limit)
            ->get()
            ->toArray();

        $outData = [];

        foreach ($tasks as $key => $task) {
            $outData[] = [
                'id' => $task['id'],
                'title' => $task['title'],
                'description' => $task['description'],
                'due_date' => $task['due_date'],
                'status' => $task['status'],
                'created_at' => $task['created_at'],
                'achieved_at' => $task['achieved_at'],
                'priority' => $task['priority'],
                'order' => $task['order'],
                'medias' => $task['medias']
            ];
        }

        $data = [
            'data' => $outData,
            'total' => $tasksCount,
            'page' => (int)$page,
            'limit' => (int)$limit,
        ];

        // return the data
        return response()->json(
            [
                'status' => 'success',
                'data' => $data
            ],
            200
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validation
        $validator = Validator::make($request->all(), [
            'title' => 'required|string|max:150|min:1',
            'description' => 'required|string|max:500|min:1',
            'due_date' => 'date_format:Y-m-d',
            'priority' => 'required ' . CommonHelper::getStringToValidationFromArrayKey(Task::PRIORITY_TYPE_TXT)
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'error' => 'Create Task Validation Error',
                'errors' => $validator->errors()
            ], 422);
        }

        // check due date validity
        if (@$request->due_date) {
            $currentDateTime = date("Y-m-d");

            if ($request->due_date < $currentDateTime) {
                return response()->json([
                    'status' => 'error',
                    'error' => 'Create Task Due Date should be greater than the currect date',
                    'errors' => []
                ], 422);
            }
        }

        // get the current logged user
        $currentUser = Auth::user();

        // get user id
        $userId = $currentUser->getAttributes()['id'];

        // get count
        $tasksCount = Task::selectRaw('COUNT(id) AS count')
            ->where('user_id', "=", $userId);

        $tasksCount = $tasksCount->get()->toArray();
        $tasksCount = $tasksCount[0]['count'];

        Task::create([
            'title' => $request->title,
            'description' => $request->description,
            'due_date' => $request->due_date,
            'priority' => $request->priority,
            'order' => $tasksCount + 1,
            'status' => Task::STATUS_TYPE_TODO,
            'user_id' => $userId
        ]);

        return response()->json(['status' => 'success'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // get the current logged user
        $currentUser = Auth::user();

        // get user id
        $userId = $currentUser->getAttributes()['id'];

        $task = Task::where('id', "=", $id)->first();

        if (!$task) {
            return response()->json([
                'status' => 'error',
                'error' => 'This task is not available',
                'errors' => []
            ], 404);
        }

        if ($userId != $task->user_id) {
            return response()->json([
                'status' => 'error',
                'error' => 'This task is not allowed to view',
                'errors' => []
            ], 401);
        }

        return response()->json(
            [
                'status' => 'success',
                'data' => $task
            ],
            200
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // validation
        $validator = Validator::make($request->all(), [
            'title' => 'required|string|max:150|min:1',
            'description' => 'required|string|max:500|min:1',
            'due_date' => 'date_format:Y-m-d',
            'priority' => 'required ' . CommonHelper::getStringToValidationFromArrayKey(Task::PRIORITY_TYPE_TXT)
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'error' => 'Update Task Validation Error',
                'errors' => $validator->errors()
            ], 422);
        }

        // check due date validity
        if (@$request->due_date) {
            $currentDateTime = date("Y-m-d");

            if ($request->due_date < $currentDateTime) {
                return response()->json([
                    'status' => 'error',
                    'error' => 'Update Task Due Date should be greater than the currect date',
                    'errors' => []
                ], 422);
            }
        }

        // get the current logged user
        $currentUser = Auth::user();

        // get user id
        $userId = $currentUser->getAttributes()['id'];

        $task = Task::where('id', "=", $id)->first();

        if (!$task) {
            return response()->json([
                'status' => 'error',
                'error' => 'This task is not available',
                'errors' => []
            ], 404);
        }

        if ($userId != $task->user_id) {
            return response()->json([
                'status' => 'error',
                'error' => 'This task is not allowed to update',
                'errors' => []
            ], 401);
        }

        $task->update([
            'title' => $request->title,
            'description' => $request->description,
            'due_date' => $request->due_date,
            'priority' => $request->priority
        ]);

        return response()->json(['status' => 'success'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // get the current logged user
        $currentUser = Auth::user();

        // get user id
        $userId = $currentUser->getAttributes()['id'];

        $task = Task::where('id', "=", $id)->first();

        if (!$task) {
            return response()->json([
                'status' => 'error',
                'error' => 'This task is not available',
                'errors' => []
            ], 404);
        }

        if ($userId != $task->user_id) {
            return response()->json([
                'status' => 'error',
                'error' => 'This task is not allowed to delete',
                'errors' => []
            ], 401);
        }

        $task->delete();

        return response()->json(['status' => 'success'], 200);
    }

    /**
     * Set The Task to incomplete or complete
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function setToComplete(Request $request, $id)
    {
        // validation
        $validator = Validator::make($request->all(), [
            'status' => 'required ' . CommonHelper::getStringToValidationFromArrayKey(Task::STATUS_TXT)
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'error' => 'Update Task Validation Error',
                'errors' => $validator->errors()
            ], 422);
        }

        // get the current logged user
        $currentUser = Auth::user();

        // get user id
        $userId = $currentUser->getAttributes()['id'];

        $task = Task::where('id', "=", $id)->first();

        if (!$task) {
            return response()->json([
                'status' => 'error',
                'error' => 'This task is not available',
                'errors' => []
            ], 404);
        }

        if ($userId != $task->user_id) {
            return response()->json([
                'status' => 'error',
                'error' => 'This task is not allowed to patch',
                'errors' => []
            ], 401);
        }

        $task->update([
            'status' => $request->status,
            'completed_at' => (Task::STATUS_TYPE_COMPLETED == $request->status ? date('Y-m-d H:i:s') : null) ,
        ]);

        return response()->json(['status' => 'success'], 200);
    }

    /**
     * Upload Media
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function upload(Request $request, $id)
    {
        // validation
        $validator = Validator::make($request->all(), [
            'medias' => 'required',
            'medias.*' => 'max:100 | ' . CommonHelper::getStringToValidateMemesFromArrayKey(
                Media::getStringToValidateMemesFromArrayKey()
            )
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'error' => 'Upload Task Media Validation Error',
                'errors' => $validator->errors()
            ], 422);
        }

        // get the current logged user
        $currentUser = Auth::user();

        // get user id
        $userId = $currentUser->getAttributes()['id'];

        $task = Task::where('id', "=", $id)->first();

        if (!$task) {
            return response()->json([
                'status' => 'error',
                'error' => 'This task is not available',
                'errors' => []
            ], 404);
        }

        if ($userId != $task->user_id) {
            return response()->json([
                'status' => 'error',
                'error' => 'This task is not allowed to patch',
                'errors' => []
            ], 401);
        }

        $medias = $request->file('medias');

        foreach($medias as $media){
            $extension = strtolower($media->getClientOriginalExtension());

            $checkImagesFileExtension = in_array($extension, Media::ALLOWED_IMAGE_EXTENSIONS);
            $checkVideosFileExtension = in_array($extension, Media::ALLOWED_VIDEO_EXTENSIONS);
            $checkdDocumentFileExtension = in_array($extension, Media::ALLOWED_DOCUMENT_EXTENSIONS);

            if($checkImagesFileExtension) {
                //move the file to the storage
                $filename = $media->store(Media::FILE_SAVE_PATH);

                Media::create([
                    'file_name' => $filename,
                    'type' => Media::FILE_TYPE_IMAGES,
                    'task_id' => $task->id,
                ]);
            }

            if ($checkVideosFileExtension) {
                //move the file to the storage
                $filename = $media->store(Media::FILE_SAVE_PATH);

                Media::create([
                    'file_name' => $filename,
                    'type' => Media::FILE_TYPE_VIDEOS,
                    'task_id' => $task->id,
                ]);
            }

            if ($checkdDocumentFileExtension) {
                //move the file to the storage
                $filename =  $media->store(Media::FILE_SAVE_PATH);

                Media::create([
                    'file_name' => $filename,
                    'type' => Media::FILE_TYPE_DOCUMENTS,
                    'task_id' => $task->id,
                ]);
            }
        }

        return response()->json(['status' => 'success'], 200);
    }
}