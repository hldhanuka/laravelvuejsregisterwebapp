<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    // priority types
    public const PRIORITY_TYPE_URGENT = 1;
    public const PRIORITY_TYPE_HIGH = 2;
    public const PRIORITY_TYPE_NORMAL = 3;
    public const PRIORITY_TYPE_LOW = 4;
    public const PRIORITY_TYPE_NONE = 5;

    // status types
    public const STATUS_TYPE_TODO = 1;
    public const STATUS_TYPE_COMPLETED = 2;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'due_date',
        'status',
        'priority',
        'order',
        'achieved_at',
        'completed_at',
        'user_id',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'due_date' => 'date:Y-m-d',
        'achieved_at' => 'date:Y-m-d H:i:s',
        'completed_at' => 'date:Y-m-d H:i:s',
        'created_at' => 'date:Y-m-d H:i:s',
        'updated_at' => 'date:Y-m-d H:i:s',
    ];

    /**
     * Belongs Tickets to User
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * Has Many Medias with Task
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function medias()
    {
        return $this->hasMany(Media::class, 'task_id');
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////// Helping Methods And Variables
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // task priority types text
    public const PRIORITY_TYPE_TXT = [
        self::PRIORITY_TYPE_URGENT => "Urgent",
        self::PRIORITY_TYPE_HIGH => "High",
        self::PRIORITY_TYPE_NORMAL => "Normal",
        self::PRIORITY_TYPE_LOW => "Low",
        self::PRIORITY_TYPE_NONE => "None"
    ];

    // task status types text
    public const STATUS_TXT = [
        self::STATUS_TYPE_TODO => "ToDo",
        self::STATUS_TYPE_COMPLETED => "Completed"
    ];
}