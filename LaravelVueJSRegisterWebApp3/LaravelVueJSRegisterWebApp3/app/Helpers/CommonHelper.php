<?php

namespace App\Helpers;

/**
 * Class APIHelper
 * 
 * @package App\Helpers
 */
class CommonHelper
{
    /**
     * Getting the all availables for the validation as a string
     * 
     * @return string
     */
    public static function getStringToValidationFromArrayKey(array $array) : string
    {
        $returnString = '';

        foreach ($array as $key => $value) {
            $returnString = (!$returnString ? "| in:$key" : $returnString . ",$key");
        }

        return $returnString;
    }

    /**
     * Getting the all meme types for the validation as a string
     * 
     * @return string
     */
    public static function getStringToValidateMemesFromArrayKey(array $array) : string
    {
        $returnString = '';

        foreach ($array as $key => $value) {
            $returnString = (!$returnString ? "|mimes:$value" : $returnString . ",$value");
        }

        return $returnString;
    }
}