<?php

namespace App\Http\Controllers;

use App\Task;
use App\Media;
use Illuminate\Http\Request;
use App\Helpers\CommonHelper;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     * 
     * @param Request $request
     * @param int $page
     * @param int $limit
     *
     * @return \Illuminate\Http\Response
     * 
     * @OA\Get(
     *     path="/api/tasks/{page}/{size}",
     *     summary="Get All Tasks",
     *     tags={"Task Management APIs"},
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *          name="Content-Type",
     *          in="header",
     *          required=true,
     *          example="application/json",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Parameter(
     *         name="page",
     *         in="path",
     *         description="Pagination Page",
     *         required=true,
     *         example=1,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Parameter(
     *         name="size",
     *         in="path",
     *         description="Total Count for a Page",
     *         required=true,
     *         example=10,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Parameter(
     *         name="search",
     *         in="path",
     *         description="Search Key",
     *         required=false,
     *         example="test",
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="sort_by",
     *         in="path",
     *         description="Sort By - [desc , asc]",
     *         required=false,
     *         example="asc",
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="sort_col_name",
     *         in="path",
     *         description="Col Names - [created_at, priority, due_date, status,  description, title]",
     *         required=false,
     *         example="created_at",
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="date_col_name",
     *         in="path",
     *         description="Col Names - [due_date, created_at, achieved_at, completed_at]",
     *         required=false,
     *         example="due_date",
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="date_start",
     *         in="path",
     *         description="Start Date",
     *         required=false,
     *         example="2024-04-04",
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="date_end",
     *         in="path",
     *         description="End Date",
     *         required=false,
     *         example="2024-04-04",
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *          response="200", 
     *          description="Get All Tasks",
     *          content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="status",
     *                         type="string",
     *                         description="success"
     *                     ),
     *                     @OA\Property(
     *                         property="data",
     *                         type="array",
     *                         description="Details Array With Pagination",
     *                         @OA\Items(
     *                             type="array",
     *                             description="Task Details",
     *                             @OA\Items(
     *                                  type="string",
     *                                  description="Task Details"
     *                             )
     *                         )
     *                     ),
     *                     example={
     *                         "status": "success",
     *                         "data": {
     *                              "data": {
     *                                  "id": 1,
     *                                  "title": "test1",
     *                                  "description": "test1",
     *                                  "due_date": null,
     *                                  "status": 1,
     *                                  "created_at": "2024-04-05 19:59:47",
     *                                  "archived_at": null,
     *                                  "priority": 1,
     *                                  "order": 1,
     *                                  "medias": {
     *                                      {
     *                                          "id": 4,
     *                                          "file_name": "media/jrGvILA9m7QlMAF4bTjp6rVHdAshY0IEUYh5bqaO.txt",
     *                                          "type": 2,
     *                                          "task_id": 1,
     *                                          "created_at": "2024-04-06 08:45:59",
     *                                          "updated_at": "2024-04-06 08:45:59"
     *                                      }
     *                                  },
     *                                  "tags": {
     *                                      {
     *                                          "id": 1,
     *                                          "tag_name": "Test 1",
     *                                          "created_at": "2024-04-07 05:16:26",
     *                                          "updated_at": "2024-04-07 05:16:26",
     *                                          "pivot": {
     *                                              "task_id": 1,
     *                                              "tag_id": 1
     *                                           }
     *                                      }
     *                                  }
     *                              },
     *                              "total": 2,
     *                              "page": 1,
     *                              "limit": 10
     *                         }
     *                     }
     *                 )
     *             )
     *         }
     *     ),
     *     @OA\Response(
     *          response="401", 
     *          description="Try to get all tasks from the user already logged out",
     *          content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="error",
     *                         type="string",
     *                         description="Unauthorized"
     *                     ),
     *                     example={
     *                         "error": "Unauthorized"
     *                     }
     *                 )
     *             )
     *         }
     *     )
     * )
     * 
     */
    public function index(Request $request, $page, $limit)
    {
        // get Request Data
        $data = $request->all();

        if ($limit <= 0) {
            return response()->json([
                'status' => false,
                'error' => 'Invalid Limit'
            ], 422);
        }

        // get the current logged user
        $currentUser = Auth::user();

        // get user id
        $userId = $currentUser->getAttributes()['id'];

        // get count
        $tasksCount = Task::selectRaw('COUNT(id) AS count')
            ->where('user_id', "=", $userId);

        $allTasks = Task::select('*')->with('medias')->with('tags')->where('user_id', "=", $userId);

        // Check the Search Tags
        if (@$data['search']) {
            // get count
            $tasksCount = $tasksCount
                ->where('priority', '=', $data['search'])
                ->orWhere('status', '=', $data['search'])
                ->orWhere('title', '=', $data['search']);

            $allTasks = $allTasks
                ->where('priority', '=', $data['search'])
                ->orWhere('status', '=', $data['search'])
                ->orWhere('title', '=', $data['search']);
        }

        if (@$data['date_col_name'] && @$data['date_start'] && @$data['date_end']) {
            switch ($data['date_col_name']) {
                case 'due_date':
                    $tasksCount = $tasksCount->whereBetween('due_date', [$data['date_start'], $data['date_end']]);

                    $allTasks = $allTasks
                        ->whereBetween('due_date', [$data['date_start'], $data['date_end']]);
                    break;
                case 'created_at':
                    $tasksCount = $tasksCount->whereBetween('created_at', [$data['date_start'], $data['date_end']]);

                    $allTasks = $allTasks
                        ->whereBetween('created_at', [$data['date_start'], $data['date_end']]);
                    break;
                case 'archived_at':
                    $tasksCount = $tasksCount->whereBetween('archived_at', [$data['date_start'], $data['date_end']]);

                    $allTasks = $allTasks
                        ->whereBetween('archived_at', [$data['date_start'], $data['date_end']]);
                    break;
                case 'completed_at':
                    $tasksCount = $tasksCount->whereBetween('completed_at', [$data['date_start'], $data['date_end']]);

                    $allTasks = $allTasks
                        ->whereBetween('completed_at', [$data['date_start'], $data['date_end']]);
                    break;
                default:
                    break;
            }
        }

        if (@$data['sort_col_name'] && ((@$data['sort_by'] == 'desc') || (@$data['sort_by'] == 'asc'))) {
            switch ($data['sort_col_name']) {
                case 'created_at':
                    $allTasks = $allTasks->orderBy('created_at', $data['sort_by']);
                    break;
                case 'priority':
                    $allTasks = $allTasks->orderBy('priority', $data['sort_by']);
                    break;
                case 'due_date':
                    $allTasks = $allTasks->orderBy('due_date', $data['sort_by']);
                    break;
                case 'status':
                    $allTasks = $allTasks->orderBy('status', $data['sort_by']);
                    break;
                case 'description':
                    $allTasks = $allTasks->orderBy('description', $data['sort_by']);
                    break;
                case 'title':
                    $allTasks = $allTasks->orderBy('title', $data['sort_by']);
                    break;
                default:
                    break;
            }
        }

        $tasksCount = $tasksCount->get()->toArray();
        $tasksCount = $tasksCount[0]['count'];

        // skip calculation
        $skip = ((int)$page - 1) * (int)$limit;

        // get the list
        $tasks = $allTasks
            ->skip($skip)
            ->take($limit)
            ->get()
            ->toArray();

        $outData = [];

        foreach ($tasks as $key => $task) {
            $outData[] = [
                'id' => $task['id'],
                'title' => $task['title'],
                'description' => $task['description'],
                'due_date' => $task['due_date'],
                'status' => $task['status'],
                'created_at' => $task['created_at'],
                'archived_at' => $task['archived_at'],
                'priority' => $task['priority'],
                'order' => $task['order'],
                'medias' => $task['medias'],
                'tags' => $task['tags']
            ];
        }

        $data = [
            'data' => $outData,
            'total' => $tasksCount,
            'page' => (int)$page,
            'limit' => (int)$limit,
        ];

        // return the data
        return response()->json(
            [
                'status' => 'success',
                'data' => $data
            ],
            200
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     * 
     * @OA\Post(
     *     path="/api/tasks",
     *     summary="Save A Task With/Without Due Date",
     *     tags={"Task Management APIs"},
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *          name="Content-Type",
     *          in="header",
     *          required=true,
     *          example="application/json",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Parameter(
     *         name="title",
     *         in="query",
     *         description="Task Title Name",
     *         required=true,
     *         example="test3",
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="description",
     *         in="query",
     *         description="Task Description Name",
     *         required=true,
     *         example="test3",
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="due_date",
     *         in="query",
     *         description="Task Due Date",
     *         required=false,
     *         example="2024-04-06",
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="priority",
     *         in="query",
     *         description="Task Priority [1 - Urgent, 2 - High, 3 - Normal, 4 - Low, 5 - None]",
     *         required=true,
     *         example=1,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *          response="200", 
     *          description="Task Created successfully",
     *          content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="status",
     *                         type="string",
     *                         description="success"
     *                     ),
     *                     example={
     *                         "status": "success"
     *                     }
     *                 )
     *             )
     *         }
     *     ),
     *     @OA\Response(
     *          response="422", 
     *          description="Task Validation errors",
     *          content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="status",
     *                         type="string",
     *                         description="error"
     *                     ),
     *                     @OA\Property(
     *                         property="error",
     *                         type="string",
     *                         description="Create Task Validation Error"
     *                     ),
     *                     @OA\Property(
     *                         property="errors",
     *                         type="array",
     *                         description="Validation Errors",
     *                         @OA\Items(
     *                             type="array",
     *                             description="Validation Key Params",
     *                             @OA\Items(
     *                                 type="string",
     *                                 description="Validation Error Message"
     *                             )
     *                         )
     *                     ),
     *                     example={
     *                          "status": "error",
     *                          "error": "Create Task Validation Error",
     *                          "errors": {
     *                              "title": {
     *                                  "The title field is required."
     *                              },
     *                              "priority": {
     *                                  "The selected priority is invalid."
     *                              },
     *                              "description": {
     *                                  "The description field is required."
     *                              }
     *                          }
     *                     }
     *                 )
     *             )
     *         }
     *     ),
     *     @OA\Response(
     *          response="401", 
     *          description="Try to get all tasks from the user already logged out",
     *          content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="error",
     *                         type="string",
     *                         description="Unauthorized"
     *                     ),
     *                     example={
     *                         "error": "Unauthorized"
     *                     }
     *                 )
     *             )
     *         }
     *     )
     * )
     * 
     */
    public function store(Request $request)
    {
        // validation
        $validator = Validator::make($request->all(), [
            'title' => 'required|string|max:150|min:1',
            'description' => 'required|string|max:500|min:1',
            'due_date' => 'date_format:Y-m-d',
            'priority' => 'required ' . CommonHelper::getStringToValidationFromArrayKey(Task::PRIORITY_TYPE_TXT)
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'error' => 'Create Task Validation Error',
                'errors' => $validator->errors()
            ], 422);
        }

        // check due date validity
        if (@$request->due_date) {
            $currentDateTime = date("Y-m-d");

            if ($request->due_date < $currentDateTime) {
                return response()->json([
                    'status' => 'error',
                    'error' => 'Create Task Due Date should be greater than the currect date',
                    'errors' => []
                ], 422);
            }
        }

        // get the current logged user
        $currentUser = Auth::user();

        // get user id
        $userId = $currentUser->getAttributes()['id'];

        // get count
        $tasksCount = Task::selectRaw('COUNT(id) AS count')
            ->where('user_id', "=", $userId);

        $tasksCount = $tasksCount->get()->toArray();
        $tasksCount = $tasksCount[0]['count'];

        Task::create([
            'title' => $request->title,
            'description' => $request->description,
            'due_date' => $request->due_date,
            'priority' => $request->priority,
            'order' => $tasksCount + 1,
            'status' => Task::STATUS_TYPE_TODO,
            'user_id' => $userId
        ]);

        return response()->json(['status' => 'success'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     * 
     * @OA\Get(
     *     path="/api/tasks/{id}",
     *     summary="Get A Task By Id",
     *     tags={"Task Management APIs"},
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *          name="Content-Type",
     *          in="header",
     *          required=true,
     *          example="application/json",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="Task Id",
     *         required=true,
     *         example=1,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *          response="200", 
     *          description="Get All Tasks",
     *          content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="status",
     *                         type="string",
     *                         description="success"
     *                     ),
     *                     @OA\Property(
     *                         property="data",
     *                         type="array",
     *                         description="Details Array",
     *                         @OA\Items(
     *                             type="array",
     *                             description="Task Details",
     *                             @OA\Items(
     *                                  type="string",
     *                                  description="Task Details"
     *                             )
     *                         )
     *                     ),
     *                     example={
     *                          "status": "success",
     *                          "data": {
     *                              "id": 1,
     *                              "title": "test1",
     *                              "description": "test1",
     *                              "due_date": null,
     *                              "status": 1,
     *                              "priority": 1,
     *                              "order": 1,
     *                              "archived_at": null,
     *                              "completed_at": null,
     *                              "user_id": 1,
     *                              "created_at": "2024-04-05 19:59:47",
     *                              "updated_at": "2024-04-05 19:59:47",
     *                              "medias": {
     *                                  {
     *                                      "id": 4,
     *                                      "file_name": "media/jrGvILA9m7QlMAF4bTjp6rVHdAshY0IEUYh5bqaO.txt",
     *                                      "type": 2,
     *                                      "task_id": 1,
     *                                      "created_at": "2024-04-06 08:45:59",
     *                                      "updated_at": "2024-04-06 08:45:59"
     *                                  }
     *                              },
     *                               "tags": {
     *                                  {
     *                                      "id": 1,
     *                                      "tag_name": "Test 1",
     *                                      "created_at": "2024-04-07 05:16:26",
     *                                      "updated_at": "2024-04-07 05:16:26",
     *                                      "pivot": {
     *                                          "task_id": 1,
     *                                           "tag_id": 1
     *                                      }
     *                                  }
     *                              }
     *                          }
     *                     }
     *                 )
     *             )
     *         }
     *     ),
     *     @OA\Response(
     *          response="404", 
     *          description="When try to get a task which is not in the system",
     *          content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="status",
     *                         type="string",
     *                         description="error"
     *                     ),
     *                     @OA\Property(
     *                         property="error",
     *                         type="string",
     *                         description="The task is not available"
     *                     ),
     *                     @OA\Property(
     *                         property="errors",
     *                         type="array",
     *                         description="Errors Array",
     *                         @OA\Items(
     *                             type="array",
     *                             description="Errors Details",
     *                             @OA\Items(
     *                                 type="string",
     *                                 description="Error"
     *                             )
     *                         )
     *                     ),
     *                     example={
     *                          "status": "error",
     *                          "error": "This task is not available",
     *                          "errors": {}
     *                     }
     *                 )
     *             )
     *         }
     *     ),
     *     @OA\Response(
     *          response="401", 
     *          description="When try to get a task which is not created by the currect user",
     *          content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="status",
     *                         type="string",
     *                         description="error"
     *                     ),
     *                     @OA\Property(
     *                         property="error",
     *                         type="string",
     *                         description="The task is not allowed to view"
     *                     ),
     *                     @OA\Property(
     *                         property="errors",
     *                         type="array",
     *                         description="Errors Array",
     *                         @OA\Items(
     *                             type="array",
     *                             description="Errors Details",
     *                             @OA\Items(
     *                                 type="string",
     *                                 description="Error"
     *                             )
     *                         )
     *                     ),
     *                     example={
     *                          "status": "error",
     *                          "error": "This task is not allowed to view",
     *                          "errors": {}
     *                     }
     *                 )
     *             )
     *         }
     *     )
     * )
     * 
     */
    public function show($id)
    {
        // get the current logged user
        $currentUser = Auth::user();

        // get user id
        $userId = $currentUser->getAttributes()['id'];

        $task = Task::where('id', "=", $id)->with('medias')->with('tags')->first();

        if (!$task) {
            return response()->json([
                'status' => 'error',
                'error' => 'This task is not available',
                'errors' => []
            ], 404);
        }

        if ($userId != $task->user_id) {
            return response()->json([
                'status' => 'error',
                'error' => 'This task is not allowed to view',
                'errors' => []
            ], 401);
        }

        return response()->json(
            [
                'status' => 'success',
                'data' => $task
            ],
            200
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     * 
     * @OA\Put(
     *     path="/api/tasks/{id}",
     *     summary="Update A Task By Id",
     *     tags={"Task Management APIs"},
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *          name="Content-Type",
     *          in="header",
     *          required=true,
     *          example="application/json",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="Task Id",
     *         required=true,
     *         example=1,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Parameter(
     *         name="title",
     *         in="query",
     *         description="Task Title Name",
     *         required=true,
     *         example="test3",
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="description",
     *         in="query",
     *         description="Task Description Name",
     *         required=true,
     *         example="test3",
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="due_date",
     *         in="query",
     *         description="Task Due Date",
     *         required=false,
     *         example="2024-04-06",
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="priority",
     *         in="query",
     *         description="Task Priority [1 - Urgent, 2 - High, 3 - Normal, 4 - Low, 5 - None]",
     *         required=true,
     *         example=1,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *          response="200", 
     *          description="Task Updated successfully",
     *          content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="status",
     *                         type="string",
     *                         description="success"
     *                     ),
     *                     example={
     *                         "status": "success"
     *                     }
     *                 )
     *             )
     *         }
     *     ),
     *     @OA\Response(
     *          response="422", 
     *          description="Task Validation errors",
     *          content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="status",
     *                         type="string",
     *                         description="error"
     *                     ),
     *                     @OA\Property(
     *                         property="error",
     *                         type="string",
     *                         description="Create Task Validation Error"
     *                     ),
     *                     @OA\Property(
     *                         property="errors",
     *                         type="array",
     *                         description="Validation Errors",
     *                         @OA\Items(
     *                             type="array",
     *                             description="Validation Key Params",
     *                             @OA\Items(
     *                                 type="string",
     *                                 description="Validation Error Message"
     *                             )
     *                         )
     *                     ),
     *                     example={
     *                          "status": "error",
     *                          "error": "Create Task Validation Error",
     *                          "errors": {
     *                              "title": {
     *                                  "The title field is required."
     *                              },
     *                              "priority": {
     *                                  "The selected priority is invalid."
     *                              },
     *                              "description": {
     *                                  "The description field is required."
     *                              }
     *                          }
     *                     }
     *                 )
     *             )
     *         }
     *     ),
     *     @OA\Response(
     *          response="404", 
     *          description="When try to update a task which is not in the system",
     *          content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="status",
     *                         type="string",
     *                         description="error"
     *                     ),
     *                     @OA\Property(
     *                         property="error",
     *                         type="string",
     *                         description="The task is not available"
     *                     ),
     *                     @OA\Property(
     *                         property="errors",
     *                         type="array",
     *                         description="Errors Array",
     *                         @OA\Items(
     *                             type="array",
     *                             description="Errors Details",
     *                             @OA\Items(
     *                                 type="string",
     *                                 description="Error"
     *                             )
     *                         )
     *                     ),
     *                     example={
     *                          "status": "error",
     *                          "error": "This task is not available",
     *                          "errors": {}
     *                     }
     *                 )
     *             )
     *         }
     *     ),
     *     @OA\Response(
     *          response="401", 
     *          description="When try to update a task which is not created by the currect user",
     *          content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="status",
     *                         type="string",
     *                         description="error"
     *                     ),
     *                     @OA\Property(
     *                         property="error",
     *                         type="string",
     *                         description="The task is not allowed to view"
     *                     ),
     *                     @OA\Property(
     *                         property="errors",
     *                         type="array",
     *                         description="Errors Array",
     *                         @OA\Items(
     *                             type="array",
     *                             description="Errors Details",
     *                             @OA\Items(
     *                                 type="string",
     *                                 description="Error"
     *                             )
     *                         )
     *                     ),
     *                     example={
     *                          "status": "error",
     *                          "error": "This task is not allowed to view",
     *                          "errors": {}
     *                     }
     *                 )
     *             )
     *         }
     *     )
     * )
     * 
     */
    public function update(Request $request, $id)
    {
        // validation
        $validator = Validator::make($request->all(), [
            'title' => 'required|string|max:150|min:1',
            'description' => 'required|string|max:500|min:1',
            'due_date' => 'date_format:Y-m-d',
            'priority' => 'required ' . CommonHelper::getStringToValidationFromArrayKey(Task::PRIORITY_TYPE_TXT)
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'error' => 'Update Task Validation Error',
                'errors' => $validator->errors()
            ], 422);
        }

        // check due date validity
        if (@$request->due_date) {
            $currentDateTime = date("Y-m-d");

            if ($request->due_date < $currentDateTime) {
                return response()->json([
                    'status' => 'error',
                    'error' => 'Update Task Due Date should be greater than the currect date',
                    'errors' => []
                ], 422);
            }
        }

        // get the current logged user
        $currentUser = Auth::user();

        // get user id
        $userId = $currentUser->getAttributes()['id'];

        $task = Task::where('id', "=", $id)->first();

        if (!$task) {
            return response()->json([
                'status' => 'error',
                'error' => 'This task is not available',
                'errors' => []
            ], 404);
        }

        if ($userId != $task->user_id) {
            return response()->json([
                'status' => 'error',
                'error' => 'This task is not allowed to update',
                'errors' => []
            ], 401);
        }

        $task->update([
            'title' => $request->title,
            'description' => $request->description,
            'due_date' => $request->due_date,
            'priority' => $request->priority
        ]);

        return response()->json(['status' => 'success'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     * 
     * @OA\Delete(
     *     path="/api/tasks/{id}",
     *     summary="Update A Task By Id",
     *     tags={"Task Management APIs"},
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *          name="Content-Type",
     *          in="header",
     *          required=true,
     *          example="application/json",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="Task Id",
     *         required=true,
     *         example=1,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *          response="200", 
     *          description="Task Deleted successfully",
     *          content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="status",
     *                         type="string",
     *                         description="success"
     *                     ),
     *                     example={
     *                         "status": "success"
     *                     }
     *                 )
     *             )
     *         }
     *     ),
     *     @OA\Response(
     *          response="404", 
     *          description="When try to delete a task which is not in the system",
     *          content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="status",
     *                         type="string",
     *                         description="error"
     *                     ),
     *                     @OA\Property(
     *                         property="error",
     *                         type="string",
     *                         description="The task is not available"
     *                     ),
     *                     @OA\Property(
     *                         property="errors",
     *                         type="array",
     *                         description="Errors Array",
     *                         @OA\Items(
     *                             type="array",
     *                             description="Errors Details",
     *                             @OA\Items(
     *                                 type="string",
     *                                 description="Error"
     *                             )
     *                         )
     *                     ),
     *                     example={
     *                          "status": "error",
     *                          "error": "This task is not available",
     *                          "errors": {}
     *                     }
     *                 )
     *             )
     *         }
     *     ),
     *     @OA\Response(
     *          response="401", 
     *          description="When try to delete a task which is not created by the currect user",
     *          content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="status",
     *                         type="string",
     *                         description="error"
     *                     ),
     *                     @OA\Property(
     *                         property="error",
     *                         type="string",
     *                         description="The task is not allowed to view"
     *                     ),
     *                     @OA\Property(
     *                         property="errors",
     *                         type="array",
     *                         description="Errors Array",
     *                         @OA\Items(
     *                             type="array",
     *                             description="Errors Details",
     *                             @OA\Items(
     *                                 type="string",
     *                                 description="Error"
     *                             )
     *                         )
     *                     ),
     *                     example={
     *                          "status": "error",
     *                          "error": "This task is not allowed to view",
     *                          "errors": {}
     *                     }
     *                 )
     *             )
     *         }
     *     )
     * )
     * 
     */
    public function destroy($id)
    {
        // get the current logged user
        $currentUser = Auth::user();

        // get user id
        $userId = $currentUser->getAttributes()['id'];

        $task = Task::where('id', "=", $id)->first();

        if (!$task) {
            return response()->json([
                'status' => 'error',
                'error' => 'This task is not available',
                'errors' => []
            ], 404);
        }

        if ($userId != $task->user_id) {
            return response()->json([
                'status' => 'error',
                'error' => 'This task is not allowed to delete',
                'errors' => []
            ], 401);
        }

        $task->delete();

        return response()->json(['status' => 'success'], 200);
    }

    /**
     * Set The Task to incomplete or complete
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     * 
     * @OA\Patch(
     *     path="/api/tasks/{id}",
     *     summary="Set To Incomplete / Complete By Id",
     *     tags={"Task Management APIs"},
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *          name="Content-Type",
     *          in="header",
     *          required=true,
     *          example="application/json",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="Task Id",
     *         required=true,
     *         example=1,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Parameter(
     *         name="status",
     *         in="query",
     *         description="Task Status - [1 - To-Do, 2 - Completed]",
     *         required=true,
     *         example=1,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *          response="200", 
     *          description="Task Updated successfully",
     *          content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="status",
     *                         type="string",
     *                         description="success"
     *                     ),
     *                     example={
     *                         "status": "success"
     *                     }
     *                 )
     *             )
     *         }
     *     ),
     *     @OA\Response(
     *          response="422", 
     *          description="Task Validation errors",
     *          content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="status",
     *                         type="string",
     *                         description="error"
     *                     ),
     *                     @OA\Property(
     *                         property="error",
     *                         type="string",
     *                         description="Update Task Validation Error"
     *                     ),
     *                     @OA\Property(
     *                         property="errors",
     *                         type="array",
     *                         description="Validation Errors",
     *                         @OA\Items(
     *                             type="array",
     *                             description="Validation Key Params",
     *                             @OA\Items(
     *                                 type="string",
     *                                 description="Validation Error Message"
     *                             )
     *                         )
     *                     ),
     *                     example={
     *                          "status": "error",
     *                          "error": "Update Task Validation Error",
     *                          "errors": {
     *                              "status": {
     *                                  "The selected status is invalid."
     *                              },
     *                          }
     *                     }
     *                 )
     *             )
     *         }
     *     ),
     *     @OA\Response(
     *          response="404", 
     *          description="When try to update a task which is not in the system",
     *          content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="status",
     *                         type="string",
     *                         description="error"
     *                     ),
     *                     @OA\Property(
     *                         property="error",
     *                         type="string",
     *                         description="The task is not available"
     *                     ),
     *                     @OA\Property(
     *                         property="errors",
     *                         type="array",
     *                         description="Errors Array",
     *                         @OA\Items(
     *                             type="array",
     *                             description="Errors Details",
     *                             @OA\Items(
     *                                 type="string",
     *                                 description="Error"
     *                             )
     *                         )
     *                     ),
     *                     example={
     *                          "status": "error",
     *                          "error": "This task is not available",
     *                          "errors": {}
     *                     }
     *                 )
     *             )
     *         }
     *     ),
     *     @OA\Response(
     *          response="401", 
     *          description="When try to update a task which is not created by the currect user",
     *          content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="status",
     *                         type="string",
     *                         description="error"
     *                     ),
     *                     @OA\Property(
     *                         property="error",
     *                         type="string",
     *                         description="The task is not allowed to view"
     *                     ),
     *                     @OA\Property(
     *                         property="errors",
     *                         type="array",
     *                         description="Errors Array",
     *                         @OA\Items(
     *                             type="array",
     *                             description="Errors Details",
     *                             @OA\Items(
     *                                 type="string",
     *                                 description="Error"
     *                             )
     *                         )
     *                     ),
     *                     example={
     *                          "status": "error",
     *                          "error": "This task is not allowed to view",
     *                          "errors": {}
     *                     }
     *                 )
     *             )
     *         }
     *     )
     * )
     * 
     */
    public function setToComplete(Request $request, $id)
    {
        // validation
        $validator = Validator::make($request->all(), [
            'status' => 'required ' . CommonHelper::getStringToValidationFromArrayKey(Task::STATUS_TXT)
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'error' => 'Update Task Validation Error',
                'errors' => $validator->errors()
            ], 422);
        }

        // get the current logged user
        $currentUser = Auth::user();

        // get user id
        $userId = $currentUser->getAttributes()['id'];

        $task = Task::where('id', "=", $id)->first();

        if (!$task) {
            return response()->json([
                'status' => 'error',
                'error' => 'This task is not available',
                'errors' => []
            ], 404);
        }

        if ($userId != $task->user_id) {
            return response()->json([
                'status' => 'error',
                'error' => 'This task is not allowed to patch',
                'errors' => []
            ], 401);
        }

        $task->update([
            'status' => $request->status,
            'completed_at' => (Task::STATUS_TYPE_COMPLETED == $request->status ? date('Y-m-d H:i:s') : null) ,
        ]);

        return response()->json(['status' => 'success'], 200);
    }

    /**
     * Upload Media
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     * 
     * @OA\Post(
     *     path="/api/tasks/upload/{id}",
     *     summary="Task Media Attach By Id",
     *     tags={"Task Management APIs"},
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *          name="Content-Type",
     *          in="header",
     *          required=true,
     *          example="multipart/form-data",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="Task Id",
     *         required=true,
     *         example=1,
     *         @OA\Schema(type="integer")
     *     ),
     *      @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="medias[]",
     *                     type="array",
     *                     @OA\Items(
     *                         description="Binary content of file",
     *                         type="file",
     *                         format="binary"
     *                     )
     *                 ),
     *                 required={"medias[]"}
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *          response="200", 
     *          description="Task Media Updated successfully",
     *          content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="status",
     *                         type="string",
     *                         description="success"
     *                     ),
     *                     example={
     *                         "status": "success"
     *                     }
     *                 )
     *             )
     *         }
     *     ),
     *     @OA\Response(
     *          response="422", 
     *          description="Task Validation errors",
     *          content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="status",
     *                         type="string",
     *                         description="error"
     *                     ),
     *                     @OA\Property(
     *                         property="error",
     *                         type="string",
     *                         description="Upload Task Media Validation Error"
     *                     ),
     *                     @OA\Property(
     *                         property="errors",
     *                         type="array",
     *                         description="Validation Errors",
     *                         @OA\Items(
     *                             type="array",
     *                             description="Validation Key Params",
     *                             @OA\Items(
     *                                 type="string",
     *                                 description="Validation Error Message"
     *                             )
     *                         )
     *                     ),
     *                     example={
     *                          "status": "error",
     *                          "error": "Upload Task Media Validation Error",
     *                          "errors": {
     *                              "medias": {
     *                                   "The medias field is required."
     *                              },
     *                          }
     *                     }
     *                 )
     *             )
     *         }
     *     ),
     *     @OA\Response(
     *          response="404", 
     *          description="When try to update a task which is not in the system",
     *          content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="status",
     *                         type="string",
     *                         description="error"
     *                     ),
     *                     @OA\Property(
     *                         property="error",
     *                         type="string",
     *                         description="The task is not available"
     *                     ),
     *                     @OA\Property(
     *                         property="errors",
     *                         type="array",
     *                         description="Errors Array",
     *                         @OA\Items(
     *                             type="array",
     *                             description="Errors Details",
     *                             @OA\Items(
     *                                 type="string",
     *                                 description="Error"
     *                             )
     *                         )
     *                     ),
     *                     example={
     *                          "status": "error",
     *                          "error": "This task is not available",
     *                          "errors": {}
     *                     }
     *                 )
     *             )
     *         }
     *     ),
     *     @OA\Response(
     *          response="401", 
     *          description="When try to update a task which is not created by the currect user",
     *          content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="status",
     *                         type="string",
     *                         description="error"
     *                     ),
     *                     @OA\Property(
     *                         property="error",
     *                         type="string",
     *                         description="The task is not allowed to view"
     *                     ),
     *                     @OA\Property(
     *                         property="errors",
     *                         type="array",
     *                         description="Errors Array",
     *                         @OA\Items(
     *                             type="array",
     *                             description="Errors Details",
     *                             @OA\Items(
     *                                 type="string",
     *                                 description="Error"
     *                             )
     *                         )
     *                     ),
     *                     example={
     *                          "status": "error",
     *                          "error": "This task is not allowed to view",
     *                          "errors": {}
     *                     }
     *                 )
     *             )
     *         }
     *     )
     * )
     * 
     */
    public function upload(Request $request, $id)
    {
        // validation
        $validator = Validator::make($request->all(), [
            'medias' => 'required',
            'medias.*' => 'max:100 | ' . CommonHelper::getStringToValidateMemesFromArrayKey(
                Media::getStringToValidateMemesFromArrayKey()
            )
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'error' => 'Upload Task Media Validation Error',
                'errors' => $validator->errors()
            ], 422);
        }

        // get the current logged user
        $currentUser = Auth::user();

        // get user id
        $userId = $currentUser->getAttributes()['id'];

        $task = Task::where('id', "=", $id)->with('medias')->first();

        if (!$task) {
            return response()->json([
                'status' => 'error',
                'error' => 'This task is not available',
                'errors' => []
            ], 404);
        }

        if ($userId != $task->user_id) {
            return response()->json([
                'status' => 'error',
                'error' => 'This task is not allowed to patch',
                'errors' => []
            ], 401);
        }

        // get media files from the request
        $medias = $request->file('medias');

        // get old medias
        $oldMedias = $task->medias()->get();

        // remove old medias with attachements
        foreach ($oldMedias as $key => $oldMedia) {
            $fileName = $oldMedia->file_name;

            if(File::exists(Media::getMediaFilePath($fileName))) {
                File::delete(Media::getMediaFilePath($fileName));
            }

            $oldMedia->delete();
        }

        foreach($medias as $media){
            $extension = strtolower($media->getClientOriginalExtension());

            $checkImagesFileExtension = in_array($extension, Media::ALLOWED_IMAGE_EXTENSIONS);
            $checkVideosFileExtension = in_array($extension, Media::ALLOWED_VIDEO_EXTENSIONS);
            $checkdDocumentFileExtension = in_array($extension, Media::ALLOWED_DOCUMENT_EXTENSIONS);

            if($checkImagesFileExtension) {
                //move the file to the storage
                $filename = $media->store(Media::FILE_SAVE_PATH);

                Media::create([
                    'file_name' => $filename,
                    'type' => Media::FILE_TYPE_IMAGES,
                    'task_id' => $task->id,
                ]);
            }

            if ($checkVideosFileExtension) {
                //move the file to the storage
                $filename = $media->store(Media::FILE_SAVE_PATH);

                Media::create([
                    'file_name' => $filename,
                    'type' => Media::FILE_TYPE_VIDEOS,
                    'task_id' => $task->id,
                ]);
            }

            if ($checkdDocumentFileExtension) {
                //move the file to the storage
                $filename =  $media->store(Media::FILE_SAVE_PATH);

                Media::create([
                    'file_name' => $filename,
                    'type' => Media::FILE_TYPE_DOCUMENTS,
                    'task_id' => $task->id,
                ]);
            }
        }

        return response()->json(['status' => 'success'], 200);
    }

    /**
     * Add Multiple Tags
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     * 
     * @OA\Post(
     *     path="/api/tasks/add-tags/{id}",
     *     summary="Set Task to Archive and Restore Back By Id",
     *     tags={"Task Management APIs"},
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *          name="Content-Type",
     *          in="header",
     *          required=true,
     *          example="application/json",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="Task Id",
     *         required=true,
     *         example=1,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Parameter(
     *         name="tag_ids",
     *         in="query",
     *         description="Tag Ids",
     *         required=true,
     *         example="{1, 2, 3}",
     *         @OA\Schema(
     *             type="array",
     *             @OA\Items(
     *                 type="integer",
     *                 description="Tag Ids"
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *          response="200", 
     *          description="Task Updated successfully",
     *          content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="status",
     *                         type="string",
     *                         description="success"
     *                     ),
     *                     example={
     *                         "status": "success"
     *                     }
     *                 )
     *             )
     *         }
     *     ),
     *     @OA\Response(
     *          response="422", 
     *          description="Task Validation errors",
     *          content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="status",
     *                         type="string",
     *                         description="error"
     *                     ),
     *                     @OA\Property(
     *                         property="error",
     *                         type="string",
     *                         description="Add Multiple Tags Validation Error"
     *                     ),
     *                     @OA\Property(
     *                         property="errors",
     *                         type="array",
     *                         description="Validation Errors",
     *                         @OA\Items(
     *                             type="array",
     *                             description="Validation Key Params",
     *                             @OA\Items(
     *                                 type="string",
     *                                 description="Validation Error Message"
     *                             )
     *                         )
     *                     ),
     *                     example={
     *                          "status": "error",
     *                          "error": "Add Multiple Tags Validation Error",
     *                          "errors": {
     *                              "tag_ids": {
     *                                   "The tag ids field is required."
     *                              },
     *                          }
     *                     }
     *                 )
     *             )
     *         }
     *     ),
     *     @OA\Response(
     *          response="404", 
     *          description="When try to update a task which is not in the system",
     *          content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="status",
     *                         type="string",
     *                         description="error"
     *                     ),
     *                     @OA\Property(
     *                         property="error",
     *                         type="string",
     *                         description="The task is not available"
     *                     ),
     *                     @OA\Property(
     *                         property="errors",
     *                         type="array",
     *                         description="Errors Array",
     *                         @OA\Items(
     *                             type="array",
     *                             description="Errors Details",
     *                             @OA\Items(
     *                                 type="string",
     *                                 description="Error"
     *                             )
     *                         )
     *                     ),
     *                     example={
     *                          "status": "error",
     *                          "error": "This task is not available",
     *                          "errors": {}
     *                     }
     *                 )
     *             )
     *         }
     *     ),
     *     @OA\Response(
     *          response="401", 
     *          description="When try to update a task which is not created by the currect user",
     *          content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="status",
     *                         type="string",
     *                         description="error"
     *                     ),
     *                     @OA\Property(
     *                         property="error",
     *                         type="string",
     *                         description="The task is not allowed to view"
     *                     ),
     *                     @OA\Property(
     *                         property="errors",
     *                         type="array",
     *                         description="Errors Array",
     *                         @OA\Items(
     *                             type="array",
     *                             description="Errors Details",
     *                             @OA\Items(
     *                                 type="string",
     *                                 description="Error"
     *                             )
     *                         )
     *                     ),
     *                     example={
     *                          "status": "error",
     *                          "error": "This task is not allowed to view",
     *                          "errors": {}
     *                     }
     *                 )
     *             )
     *         }
     *     )
     * )
     * 
     */
    public function addMultipleTags(Request $request, $id)
    {
        // validation
        $validator = Validator::make($request->all(), [
            'tag_ids' => 'required|array|min:1'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'error' => 'Add Multiple Tags Validation Error',
                'errors' => $validator->errors()
            ], 422);
        }

        // get the current logged user
        $currentUser = Auth::user();

        // get user id
        $userId = $currentUser->getAttributes()['id'];

        $task = Task::where('id', "=", $id)->first();

        if (!$task) {
            return response()->json([
                'status' => 'error',
                'error' => 'This task is not available',
                'errors' => []
            ], 404);
        }

        if ($userId != $task->user_id) {
            return response()->json([
                'status' => 'error',
                'error' => 'This task is not allowed to patch',
                'errors' => []
            ], 401);
        }

        // get tages from the request
        $tags = $request->tag_ids;

        // delete old tags from the task
        $task->tags()->detach($tags);

        // add new tags to task
        $task->tags()->attach($tags);

        return response()->json(['status' => 'success'], 200);
    }

    /**
     * Set The Task to Archive and Restore Back
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     * 
     * @OA\Patch(
     *     path="/api/tasks/achieve/{id}",
     *     summary="Set Task to Archive and Restore Back By Id",
     *     tags={"Task Management APIs"},
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *          name="Content-Type",
     *          in="header",
     *          required=true,
     *          example="application/json",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="Task Id",
     *         required=true,
     *         example=1,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Parameter(
     *         name="is_achived",
     *         in="query",
     *         description="Task Archived - [1 - Archived, 2 - Restored]",
     *         required=true,
     *         example=true,
     *         @OA\Schema(type="boolean")
     *     ),
     *     @OA\Response(
     *          response="200", 
     *          description="Task Updated successfully",
     *          content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="status",
     *                         type="string",
     *                         description="success"
     *                     ),
     *                     example={
     *                         "status": "success"
     *                     }
     *                 )
     *             )
     *         }
     *     ),
     *     @OA\Response(
     *          response="422", 
     *          description="Task Validation errors",
     *          content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="status",
     *                         type="string",
     *                         description="error"
     *                     ),
     *                     @OA\Property(
     *                         property="error",
     *                         type="string",
     *                         description="Update Task Validation Error"
     *                     ),
     *                     @OA\Property(
     *                         property="errors",
     *                         type="array",
     *                         description="Validation Errors",
     *                         @OA\Items(
     *                             type="array",
     *                             description="Validation Key Params",
     *                             @OA\Items(
     *                                 type="string",
     *                                 description="Validation Error Message"
     *                             )
     *                         )
     *                     ),
     *                     example={
     *                          "status": "error",
     *                          "error": "Update Task Validation Error",
     *                          "errors": {
     *                              "is_achived": {
     *                                   "The is achived field is required."
     *                              },
     *                          }
     *                     }
     *                 )
     *             )
     *         }
     *     ),
     *     @OA\Response(
     *          response="404", 
     *          description="When try to update a task which is not in the system",
     *          content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="status",
     *                         type="string",
     *                         description="error"
     *                     ),
     *                     @OA\Property(
     *                         property="error",
     *                         type="string",
     *                         description="The task is not available"
     *                     ),
     *                     @OA\Property(
     *                         property="errors",
     *                         type="array",
     *                         description="Errors Array",
     *                         @OA\Items(
     *                             type="array",
     *                             description="Errors Details",
     *                             @OA\Items(
     *                                 type="string",
     *                                 description="Error"
     *                             )
     *                         )
     *                     ),
     *                     example={
     *                          "status": "error",
     *                          "error": "This task is not available",
     *                          "errors": {}
     *                     }
     *                 )
     *             )
     *         }
     *     ),
     *     @OA\Response(
     *          response="401", 
     *          description="When try to update a task which is not created by the currect user",
     *          content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="status",
     *                         type="string",
     *                         description="error"
     *                     ),
     *                     @OA\Property(
     *                         property="error",
     *                         type="string",
     *                         description="The task is not allowed to view"
     *                     ),
     *                     @OA\Property(
     *                         property="errors",
     *                         type="array",
     *                         description="Errors Array",
     *                         @OA\Items(
     *                             type="array",
     *                             description="Errors Details",
     *                             @OA\Items(
     *                                 type="string",
     *                                 description="Error"
     *                             )
     *                         )
     *                     ),
     *                     example={
     *                          "status": "error",
     *                          "error": "This task is not allowed to view",
     *                          "errors": {}
     *                     }
     *                 )
     *             )
     *         }
     *     )
     * )
     * 
     */
    public function setToArchive(Request $request, $id)
    {
        // validation
        $validator = Validator::make($request->all(), [
            'is_archived' => 'required|boolean'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'error' => 'Update Task Validation Error',
                'errors' => $validator->errors()
            ], 422);
        }

        // get the current logged user
        $currentUser = Auth::user();

        // get user id
        $userId = $currentUser->getAttributes()['id'];

        $task = Task::where('id', "=", $id)->first();

        if (!$task) {
            return response()->json([
                'status' => 'error',
                'error' => 'This task is not available',
                'errors' => []
            ], 404);
        }

        if ($userId != $task->user_id) {
            return response()->json([
                'status' => 'error',
                'error' => 'This task is not allowed to patch',
                'errors' => []
            ], 401);
        }

        $task->update([
            'archived_at' => ($request->is_archived ? date('Y-m-d H:i:s') : null),
        ]);

        return response()->json(['status' => 'success'], 200);
    }
}