<?php

namespace App\Http\Controllers;

use App\Tag;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     * 
     * @return \Illuminate\Http\Response
     * 
     * @OA\Get(
     *     path="/api/tags",
     *     summary="Get All Tags",
     *     tags={"Tag Management APIs"},
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *          name="Content-Type",
     *          in="header",
     *          required=true,
     *          example="application/json",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *          response="200", 
     *          description="Get All Taga",
     *          content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="status",
     *                         type="string",
     *                         description="success"
     *                     ),
     *                     @OA\Property(
     *                         property="tags",
     *                         type="array",
     *                         description="Tag Details Array",
     *                         @OA\Items(
     *                             type="array",
     *                             description="Tag Details Key",
     *                             @OA\Items(
     *                                  type="string",
     *                                  description="Tag Details"
     *                             )
     *                         )
     *                     ),
     *                     example={
     *                         "status": "success",
     *                         "tags": {
     *                              {
     *                                  "id": 1,
     *                                  "tag_name": "Tag 1",
     *                                  "created_at": "2024-04-03T16:21:21.000000Z",
     *                                  "updated_at": "2024-04-03T16:21:21.000000Z"
     *                              }
     *                         }
     *                     }
     *                 )
     *             )
     *         }
     *     ),
     *     @OA\Response(
     *          response="401", 
     *          description="Try to get all tasks from the user already logged out",
     *          content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *                 @OA\Schema(
     *                     @OA\Property(
     *                         property="error",
     *                         type="string",
     *                         description="Unauthorized"
     *                     ),
     *                     example={
     *                         "error": "Unauthorized"
     *                     }
     *                 )
     *             )
     *         }
     *     )
     * )
     * 
     */
    public function index()
    {
        $users = Tag::all();

        return response()->json(
            [
                'status' => 'success',
                'tags' => $users->toArray()
            ],
            200
        );
    }
}