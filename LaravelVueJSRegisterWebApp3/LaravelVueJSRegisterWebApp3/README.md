
## Installation

### Step 1

#### Package install

Run the command : 

- The Laravel package will automatically register itself, so you can start using it immediately.

```shell
composer install
```

- The npm package will automatically register itself, so you can start using it immediately.

```shell
npm install
```

#### ENV File

Create .env file : 

- Rename this file .env.example as .env or Create a .env file and copy the content of .env.example

#### Key generate

Run the command : 

- The Laravel key generate immediately.

```shell
php artisan key:generate
```

#### Storage Link to Public (create the symbolic link)

Run the command : 

- The Laravel link the storage folder to public.

```shell
php artisan storage:link
```

### Step 2 - SetUp database

- Open .env file set DB_DATABASE, DB_USERNAME, DB_PASSWORD and add MIX_APP_URL="${APP_URL}"
- Run the command : 

```shell
composer dump-autoload
```

```shell
php artisan migrate:refresh --seed
```

-The jwt token package will automatically register itself, so you can start using it immediately.

```shell
php artisan jwt:secret
```

## Usage

- Run the command on 1st shell: 

```shell
php artisan serve
```

- Run the command on 2nd shell: 

```shell
npm run watch
```

- Run the command on 3rd shell (To run the schedules): 

```shell
php artisan schedule:run
```

#### Username & password

- Username : hldhanuka@gmail.com
- Password : 123456

#### API Documentation With Swagger

- Can run whole API Collection via Swagger API Doc 
- After running the application, can access the Swagger API Documentation
- http://127.0.0.1:8000/api/documentation#/

#### API Postman Collection

- https://api.postman.com/collections/5550492-dfc4928b-c6f4-4a7f-979f-8e4defac1a18?access_key=PMAT-01HTW75ZGN9V9BKDHNTHY52WTN